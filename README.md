# Project Description
This repo is just a helper to run the `qmlformat` binary (which a user has installed locally) with pre-commit.

# Usage
- ensure that qmlformat is installed and in your path
- add to `.pre-commit-config.yaml`

